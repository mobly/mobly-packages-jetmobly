import Vue from 'vue';
import { BootstrapVue } from 'bootstrap-vue';
import App from './App.vue';
import store from './store';
import mock from './mock.js';
import LazyLoadDirective from './directives/LazyLoadDirective';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);
Vue.use(LazyLoadDirective);

new Vue({
  store,
  propsData: {
    msg: 'hello',
  },
  render: h =>
    h(App, {
      props: {
        products: mock,
        title: 'JetMobly!',
        link: '#',
      },
    }),
}).$mount('#app');
