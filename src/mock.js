export default {
  AC967UP20DSXMOB: {
    id: 178279,
    sku: 'AC967UP20DSXMOB',
    skuVariation: 'AC967UP20DSXMOB-181802',
    cartRules: {
      pixDiscount: {
        active: true,
        ruleName: 'desconto=pix',
        displayName: 'desconto=pix',
        description: 'pix',
        discountPercentage: '10',
        price: 1943.991,
      },
      creditCard1xDiscount: {
        active: true,
        ruleName: 'desconto=creditCard1x',
        displayName: 'desconto=creditCard1x',
        description: '1x no cartão de crédito',
        discountPercentage: '10',
        price: 1943.991,
      },
      lowestCartRulesByPrice: {
        hasCartRulesActive: true,
        lowestPriceAvailable: 1943.991,
        disclaimerTxt:
          'pix, 1x no cartão de crédito, nupay, boleto ou multiplos cartões.',
      },
    },
    isBundle: false,
    name: 'Sofá 3 Lugares Kivik Linho Cinza',
    url: '/sofa-3-lugares-kivik-linho-cinza-178279.html#a=3|p=1|pn=1|t=campanha-20200414-86uqch1|b=1|s=0',
    sealBlackFriday: false,
    sealExclusive: false,
    sealMarketing: false,
    description:
      '<article><h2>Por que o Sofá Kivik?</h2><p> Que tal deixar o seu momento de lazer ou descanso ainda mais agradável com o confortável <strong>Sofá Kivik</strong>? Ele é perfeito para deixar a sua casa completa e garantir momentos relaxantes até mesmo após aquele dia estressante de trabalho! O móvel deixa o ambiente muito mais <strong>aconchegante</strong> e sofisticado. Com modelo charmoso na cor cinza, agrega uma decoração mais elegante e luxuosa ao espaço. Agora é só reunir o pessoal e aproveitar! ;) </p></article>',
    seal_3d: false,
    sold_and_delivered_by: false,
    price: '2399,99',
    finalPrice: '2159,99',
    specialPrice: 0,
    installments: {
      count: '10',
      value: '216,00',
    },
    productImage: {
      optionTwo: {
        main: '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      optionThree: {
        main: '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      optionFour: {
        main: '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      collection: [
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        },
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
        },
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
        },
      ],
    },
    productAttribute: {
      color: {
        color: 'Cinza',
        label: '',
      },
      weight: {
        product_weight: '52.000',
        label: '',
      },
      height: {
        product_height: '85',
        label: '',
      },
      width: {
        product_width: '200',
        label: '',
      },
      length: {
        product_length: '80',
        label: '',
      },
      warranty: {
        product_warranty: '6 Meses',
        label: '',
      },
      description: {
        description:
          '<article><h2>Por que o Sofá Kivik?</h2><p> Que tal deixar o seu momento de lazer ou descanso ainda mais agradável com o confortável <strong>Sofá Kivik</strong>? Ele é perfeito para deixar a sua casa completa e garantir momentos relaxantes até mesmo após aquele dia estressante de trabalho! O móvel deixa o ambiente muito mais <strong>aconchegante</strong> e sofisticado. Com modelo charmoso na cor cinza, agrega uma decoração mais elegante e luxuosa ao espaço. Agora é só reunir o pessoal e aproveitar! ;) </p></article>',
        label: '',
      },
      contents: {
        product_contents: '1 Sofá + 2 Almofadas',
        label: '',
      },
      model: {
        model: 'Acqua Slim 09',
        label: '',
      },
      material: {
        material:
          'Estrutura em madeira, Assento e braços espumas D33, Encosto D23, tecido composto por 100% poliester.',
        label: '',
      },
      size_description: {
        size_description: 'Altura: 85 cm Largura 200 cm Profundidade: 80 cm',
        label: '',
      },
      assembly_required: {
        assembly_required: 'Não',
        label: '',
      },
      recommended_weight_up: {
        recommended_weight_up: '100.00',
        label: '',
      },
      special_description: {
        special_description:
          '<div class="conteiner special-description"> \n     <section class="grid_8 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/ambientada-acquarella.jpg" width="636" height="354"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/cabeçalho-acquarella.jpg" width="312" height="28"> \n        </section> \n\n        <section class="grid_4 mr-0">\n               <p>Se você procura transparecer em seu lar aconchego e conforto, o <strong>sofá Acquarella Slim</strong> é o modelo ideal!</p>\n               <p>Charmoso, ele possui uma estrutura reforçada feita em <strong>madeira lyptus</strong> de alta qualidade.</p>\n               <p>Seu <strong>estilo contemporâneo</strong> é priorizado com suas linhas retas e simplicidade, porém com acabamentos minuciosos e bonitos, transmitindo exatamente o que precisa após um dia cansativo.</p>\n               <p>Sua cor neutra é ótima para combinar com pisos de madeira ou laminados. Lindo, né?</p>\n               <p>Sua sala ficará completa com este móvel, pode apostar!</p> \n               </section>\n               \n               <div class="clear mb-24"></div>\n\n               <section class="grid_12 ml-0 mr-0">    \n  <section class="grid_6 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_1.jpg" width="474" height="226"> \n        </section> \n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_2.jpg" width="150" height="106"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <p class="title">Resistente</p>\n            <p>Estrutura em Lyptus, madeira de reflorestamento de excelente qualidade, <strong>com almofadas de encostos e assentos fixos</strong> preenchidos com molas nosag, espumas e pluma siliconada.</p>\n        </section>\n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_4.jpg" width="150" height="106"> \n        </section> \n\n        <section class="grid_4 mr-0 mt-10"> \n            <p class="title">Bem confortável</p>\n            <p>Os braços possuem a altura ideal para proporcionar bem-estar e são preenchidos com <strong>espumas D-33 e pluma siliconada</strong>.</p>\n        </section>\n\n        <section class="grid_12 ml-0 mr-0">\n\n           <section class="grid_6 ml-0 mt-10"> \n            <p>As dimensões são imprescindíveis para que você escolha o modelo ideal para o espaço que tem.</p>\n<p>Escolha o modelo de <strong>3 lugares</strong> e acomode a todos.</p> <p>Você poderá receber suas visitas ou assistir a um filme com a família confortavelmente e muito bem acomodado.</p>\n</section>\n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_5.jpg" width="150" height="150"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <p class="title">Acabamento perfeito</p>\n            <p>Os pés são produzidos em <strong>madeira tauarí</strong>, que é resistente e dá ao móvel um acabamento moderno, além de ser ecologicamente correta.</p>\n        </section>\n\n        <div class="clear mb-24"></div>\n\n<section class="grid_4 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/RI838UP45ZJQMOB_6.jpg" width="960" height="26"> \n        </section> \n\n        <div class="clear mb-24"></div>\n\n        <section class="grid_4 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-1.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-2.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-3.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4 ml-0 mt-10"> \n        <p>A iluminação da sala deve ser baixa, por isso, uma ótima opção são as <strong>luminárias</strong>. Diferentes modelos auxiliam a compor o ambiente de maneira harmoniosa.</p>\n        </section>\n\n        <section class="grid_4 mt-10"> \n        <p>As <strong>mesas de centro ou mesas de canto</strong> dão suporte ao ambiente para que você consiga deixá-lo ainda mais pessoal com objetos decorativos ou que precise ter por perto.</p>\n        </section>\n\n  <section class="grid_4 mr-0 mt-10"> \n        <p>Os <strong>puffs</strong> são móveis confortáveis e pra lá de charmosos, suas formas variadas e estampas diferentes deixam o cômodo com um visual maravilhoso.</p>\n        </section>\n</section></section></div>',
        label: '',
      },
    },
    bucket: 1,
    decomposableBundle: false,
    groupCollection: [
      {
        link: 'https://alice.mobly.local/sofa-3-lugares-kivik-linho-bege-178277.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Bege-4761-772871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Bege-4761-772871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-bege-178278.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Bege-4760-872871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Bege-4760-872871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-cinza-178280.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Cinza-4758-082871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Cinza-4758-082871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-3-lugares-kivik-linho-caqui-178281.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Caqui-6201-182871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Caqui-6201-182871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-caqui-178282.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Caqui-3638-282871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Caqui-3638-282871-sprite-cart.jpg',
        },
      },
    ],
    hasComplementary: 0,
    immediateDelivery: true,
    ratingAggregatedTotal: '4.6',
    productVarsPrice: '2399,99',
    rawPrice: '2399.99',
    extraClass: '',
    stockAvailable: true,
    buyAndEarn: 0,
    new: false,
    starPercent: 92,
    savingPercentage: '9%',
    sealInternational: false,
    sealSaldaoMobly: false,
  },
  AC997UP20DSXMOB: {
    id: 178279,
    sku: 'AC997UP20DSXMOB',
    skuVariation: 'AC997UP20DSXMOB-181802',
    cartRules: {
      pixDiscount: {
        active: true,
        ruleName: 'desconto=pix',
        displayName: 'desconto=pix',
        description: 'pix',
        discountPercentage: '50',
        price: 1199.995,
      },
      creditCard1xDiscount: {
        active: true,
        ruleName: 'desconto=creditCard1x',
        displayName: 'desconto=creditCard1x',
        description: '1x no cartão de crédito',
        discountPercentage: '10',
        price: 2159.991,
      },
      lowestCartRulesByPrice: {
        hasCartRulesActive: true,
        lowestPriceAvailable: 1199.995,
        disclaimerTxt: 'pix ',
      },
    },
    isBundle: false,
    name: 'Sofá 3 Lugares Kivik Linho Cinza',
    url: '/sofa-3-lugares-kivik-linho-cinza-178279.html#a=3|p=1|pn=1|t=campanha-20200414-86uqch1|b=1|s=0',
    sealBlackFriday: false,
    sealExclusive: false,
    sealMarketing: false,
    description:
      '<article><h2>Por que o Sofá Kivik?</h2><p> Que tal deixar o seu momento de lazer ou descanso ainda mais agradável com o confortável <strong>Sofá Kivik</strong>? Ele é perfeito para deixar a sua casa completa e garantir momentos relaxantes até mesmo após aquele dia estressante de trabalho! O móvel deixa o ambiente muito mais <strong>aconchegante</strong> e sofisticado. Com modelo charmoso na cor cinza, agrega uma decoração mais elegante e luxuosa ao espaço. Agora é só reunir o pessoal e aproveitar! ;) </p></article>',
    seal_3d: false,
    sold_and_delivered_by: false,
    price: null,
    finalPrice: '2399,99',
    specialPrice: 0,
    installments: {
      count: '10',
      value: '240,00',
    },
    productImage: {
      optionTwo: {
        main: '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      optionThree: {
        main: '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      optionFour: {
        main: '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        sprite:
          '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-sprite',
      },
      collection: [
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4759-972871-1',
        },
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-4951-972871-2',
        },
        {
          optionTwo:
            '//static.mobly.com.br/r/341x341/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
          optionThree:
            '//static.mobly.com.br/r/304x304/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
          optionFour:
            '//static.mobly.com.br/r/222x222/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Cinza-1126-972871-3',
        },
      ],
    },
    productAttribute: {
      color: {
        color: 'Cinza',
        label: '',
      },
      weight: {
        product_weight: '52.000',
        label: '',
      },
      height: {
        product_height: '85',
        label: '',
      },
      width: {
        product_width: '200',
        label: '',
      },
      length: {
        product_length: '80',
        label: '',
      },
      warranty: {
        product_warranty: '6 Meses',
        label: '',
      },
      description: {
        description:
          '<article><h2>Por que o Sofá Kivik?</h2><p> Que tal deixar o seu momento de lazer ou descanso ainda mais agradável com o confortável <strong>Sofá Kivik</strong>? Ele é perfeito para deixar a sua casa completa e garantir momentos relaxantes até mesmo após aquele dia estressante de trabalho! O móvel deixa o ambiente muito mais <strong>aconchegante</strong> e sofisticado. Com modelo charmoso na cor cinza, agrega uma decoração mais elegante e luxuosa ao espaço. Agora é só reunir o pessoal e aproveitar! ;) </p></article>',
        label: '',
      },
      contents: {
        product_contents: '1 Sofá + 2 Almofadas',
        label: '',
      },
      model: {
        model: 'Acqua Slim 09',
        label: '',
      },
      material: {
        material:
          'Estrutura em madeira, Assento e braços espumas D33, Encosto D23, tecido composto por 100% poliester.',
        label: '',
      },
      size_description: {
        size_description: 'Altura: 85 cm Largura 200 cm Profundidade: 80 cm',
        label: '',
      },
      assembly_required: {
        assembly_required: 'Não',
        label: '',
      },
      recommended_weight_up: {
        recommended_weight_up: '100.00',
        label: '',
      },
      special_description: {
        special_description:
          '<div class="conteiner special-description"> \n     <section class="grid_8 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/ambientada-acquarella.jpg" width="636" height="354"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/cabeçalho-acquarella.jpg" width="312" height="28"> \n        </section> \n\n        <section class="grid_4 mr-0">\n               <p>Se você procura transparecer em seu lar aconchego e conforto, o <strong>sofá Acquarella Slim</strong> é o modelo ideal!</p>\n               <p>Charmoso, ele possui uma estrutura reforçada feita em <strong>madeira lyptus</strong> de alta qualidade.</p>\n               <p>Seu <strong>estilo contemporâneo</strong> é priorizado com suas linhas retas e simplicidade, porém com acabamentos minuciosos e bonitos, transmitindo exatamente o que precisa após um dia cansativo.</p>\n               <p>Sua cor neutra é ótima para combinar com pisos de madeira ou laminados. Lindo, né?</p>\n               <p>Sua sala ficará completa com este móvel, pode apostar!</p> \n               </section>\n               \n               <div class="clear mb-24"></div>\n\n               <section class="grid_12 ml-0 mr-0">    \n  <section class="grid_6 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_1.jpg" width="474" height="226"> \n        </section> \n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_2.jpg" width="150" height="106"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <p class="title">Resistente</p>\n            <p>Estrutura em Lyptus, madeira de reflorestamento de excelente qualidade, <strong>com almofadas de encostos e assentos fixos</strong> preenchidos com molas nosag, espumas e pluma siliconada.</p>\n        </section>\n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_4.jpg" width="150" height="106"> \n        </section> \n\n        <section class="grid_4 mr-0 mt-10"> \n            <p class="title">Bem confortável</p>\n            <p>Os braços possuem a altura ideal para proporcionar bem-estar e são preenchidos com <strong>espumas D-33 e pluma siliconada</strong>.</p>\n        </section>\n\n        <section class="grid_12 ml-0 mr-0">\n\n           <section class="grid_6 ml-0 mt-10"> \n            <p>As dimensões são imprescindíveis para que você escolha o modelo ideal para o espaço que tem.</p>\n<p>Escolha o modelo de <strong>3 lugares</strong> e acomode a todos.</p> <p>Você poderá receber suas visitas ou assistir a um filme com a família confortavelmente e muito bem acomodado.</p>\n</section>\n\n        <section class="grid_2 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/AC967UP20DSXMOB_5.jpg" width="150" height="150"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <p class="title">Acabamento perfeito</p>\n            <p>Os pés são produzidos em <strong>madeira tauarí</strong>, que é resistente e dá ao móvel um acabamento moderno, além de ser ecologicamente correta.</p>\n        </section>\n\n        <div class="clear mb-24"></div>\n\n<section class="grid_4 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/RI838UP45ZJQMOB_6.jpg" width="960" height="26"> \n        </section> \n\n        <div class="clear mb-24"></div>\n\n        <section class="grid_4 ml-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-1.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-2.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4 mr-0"> \n            <img src="http://static.mobly.com.br/cms/conteudo/decor-3.jpg" width="312" height="312"> \n        </section> \n\n        <section class="grid_4 ml-0 mt-10"> \n        <p>A iluminação da sala deve ser baixa, por isso, uma ótima opção são as <strong>luminárias</strong>. Diferentes modelos auxiliam a compor o ambiente de maneira harmoniosa.</p>\n        </section>\n\n        <section class="grid_4 mt-10"> \n        <p>As <strong>mesas de centro ou mesas de canto</strong> dão suporte ao ambiente para que você consiga deixá-lo ainda mais pessoal com objetos decorativos ou que precise ter por perto.</p>\n        </section>\n\n  <section class="grid_4 mr-0 mt-10"> \n        <p>Os <strong>puffs</strong> são móveis confortáveis e pra lá de charmosos, suas formas variadas e estampas diferentes deixam o cômodo com um visual maravilhoso.</p>\n        </section>\n</section></section></div>',
        label: '',
      },
    },
    bucket: 1,
    decomposableBundle: false,
    groupCollection: [
      {
        link: 'https://alice.mobly.local/sofa-3-lugares-kivik-linho-bege-178277.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Bege-4761-772871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Bege-4761-772871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-bege-178278.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Bege-4760-872871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Bege-4760-872871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-cinza-178280.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Cinza-4758-082871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Cinza-4758-082871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-3-lugares-kivik-linho-caqui-178281.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Caqui-6201-182871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-3-Lugares-Kivik-Linho-Caqui-6201-182871-sprite-cart.jpg',
        },
      },
      {
        link: 'https://alice.mobly.local/sofa-2-lugares-kivik-linho-caqui-178282.html?related-catalog=AC967UP20DSXMOB#t=catalog-group',
        image: {
          main: '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Caqui-3638-282871-1-cart.jpg',
          sprite:
            '//static.mobly.com.br/p/Mobly-SofC3A1-2-Lugares-Kivik-Linho-Caqui-3638-282871-sprite-cart.jpg',
        },
      },
    ],
    hasComplementary: 0,
    immediateDelivery: true,
    ratingAggregatedTotal: '4.6',
    productVarsPrice: '2399,99',
    rawPrice: '2399.99',
    extraClass: '',
    stockAvailable: true,
    buyAndEarn: 0,
    new: false,
    starPercent: 92,
    savingPercentage: null,
    sealInternational: false,
    sealSaldaoMobly: false,
  },
  AU558LI16BXVMOB: {
    id: 229783,
    sku: 'AU558LI16BXVMOB',
    skuVariation: 'AU558LI16BXVMOB-234770',
    isBundle: false,
    name: 'Pendente Triplo 9022 Vermelho',
    url: '/pendente-triplo-9022-vermelho-229783.html#a=3|p=48|pn=1|t=vitrine-home-manual-1|b=2|s=0',
    sealBlackFriday: false,
    sealExclusive: false,
    sealMarketing: false,
    description:
      '<article><h2>Por que o Pendente Triplo 9022?</h2><p> Quer levar um pouco de <strong>cor</strong> para a sua sala, quarto ou qualquer outro ambiente da sua casa? Então que tal adquirir o <strong>Pendente Triplo 9022</strong>? Ele é perfeito para <strong>iluminar</strong> qualquer espaço, contando com um design atraente na <strong>cor vermelha</strong>. É o item que faltava para deixar o seu lar mais acolhedor, alegre e receptivo! ;) </p></article>',
    seal_3d: false,
    sold_and_delivered_by: false,
    price: null,
    finalPrice: '329,99',
    specialPrice: 0,
    installments: {
      count: 6,
      value: '55,00',
    },
    productImage: {
      optionTwo: {
        main: '//static.mobly.local/r/341x341/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
        sprite:
          '//static.mobly.local/r/341x341/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-sprite',
      },
      optionThree: {
        main: '//static.mobly.local/r/304x304/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
        sprite:
          '//static.mobly.local/r/304x304/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-sprite',
      },
      optionFour: {
        main: '//static.mobly.local/r/222x222/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
        sprite:
          '//static.mobly.local/r/222x222/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-sprite',
      },
      collection: [
        {
          optionTwo:
            '//static.mobly.local/r/341x341/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
          optionThree:
            '//static.mobly.local/r/304x304/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
          optionFour:
            '//static.mobly.local/r/222x222/p/Auremar-Pendente-Triplo-9022-Vermelho-5903-387922-1',
        },
      ],
    },
    productAttribute: {
      color: {
        color: 'Vermelho',
        label: 'Cor',
      },
      weight: {
        product_weight: '1.750',
      },
      height: {
        product_height: '88',
      },
      width: {
        product_width: '38',
      },
      length: {
        product_length: '38',
      },
      product_warranty: {
        product_warranty: '6 Meses',
        label: 'Garantia',
      },
      description: {
        description:
          '<article><h2>Por que o Pendente Triplo 9022?</h2><p> Quer levar um pouco de <strong>cor</strong> para a sua sala, quarto ou qualquer outro ambiente da sua casa? Então que tal adquirir o <strong>Pendente Triplo 9022</strong>? Ele é perfeito para <strong>iluminar</strong> qualquer espaço, contando com um design atraente na <strong>cor vermelha</strong>. É o item que faltava para deixar o seu lar mais acolhedor, alegre e receptivo! ;) </p></article>',
        label: 'Descrição',
      },
      product_contents: {
        product_contents: '1 PENDENTE',
        label: 'Conteúdo da Embalagem',
      },
      application: {
        application: 'Sala de Estar',
        label: 'Ambientes',
      },
      model: {
        model: '9022/3 VM',
        label: 'Modelo',
      },
      material: {
        material: 'BASE EM ALUMINIO NA COR BRANCA E VIDRO TEMPERADO COLORIDO',
        label: 'Material',
      },
      size_description: {
        size_description: 'Altura: 88 cm  Largura: 38 cm  Profundidade: 38 cm',
        label: 'Descrição do Tamanho',
      },
      product_height: {
        product_height: '88',
        label: 'Altura do Produto (cm)',
      },
      product_width: {
        product_width: '38',
        label: 'Largura do Produto (cm)',
      },
      product_length: {
        product_length: '38',
        label: 'Comprimento do Produto (cm)',
      },
      product_weight: {
        product_weight: '1.750',
        label: 'Peso do Produto (Kg)',
      },
      assembly_required: {
        assembly_required: 'Não',
        label: 'Necessita Montagem?',
      },
      light_lamp_type: {
        light_lamp_type: 'LED',
        label: 'Tipo de Lâmpada',
      },
      format_gl: {
        format_gl: 'Outro formato',
        label: 'Formato',
      },
      watt: {
        watt: '20 - 49W',
        label: 'Potência (W)',
      },
      socket_type: {
        socket_type: 'E27',
        label: 'Tipo do Soquete',
      },
    },
    bucket: 2,
    decomposableBundle: false,
    groupCollection: [],
    hasComplementary: 0,
    immediateDelivery: false,
    ratingAggregatedTotal: 3,
    productVarsPrice: '329,99',
    rawPrice: '329.99',
    extraClass: '',
    stockAvailable: 0,
    buyAndEarn: 0,
    new: false,
    starPercent: 60,
    savingPercentage: null,
    sealInternational: false,
    sealSaldaoMobly: false,
    special_price_paypal: 0,
    saving_percentage_paypal: 0,
    saving_difference_paypal: 0,
    saving_difference_spall_source: 0,
    cartRules: {
      pixDiscount: {
        active: true,
        ruleName: 'desconto=pix',
        displayName: 'desconto=pix',
        description: 'pix',
        discountPercentage: '10',
        price: 296.99,
      },
      creditCard1xDiscount: {
        active: true,
        ruleName: 'desconto=creditCard1x',
        displayName: 'desconto=creditCard1x',
        description: '1x no cartão de crédito',
        discountPercentage: '10',
        price: 296.99,
      },
      lowestCartRulesByPrice: {
        hasCartRulesActive: true,
        lowestPriceAvailable: 296.99,
        disclaimerTxt: 'pix ou 1x no cartão de crédito ',
      },
    },
    position: 48,
  },
  JD356LI46PDXMOB: {
    id: 221153,
    sku: 'JD356LI46PDXMOB',
    skuVariation: 'JD356LI46PDXMOB-225640',
    isBundle: false,
    name: 'Plafon Embutido Quadrado 3541 1 Lâmpada  Bivolt',
    url: '/plafon-embutido-quadrado-3541-1-lampada-bivolt-221153.html#a=3|p=2|pn=1|t=vitrine-home-manual-1|b=2|s=0',
    sealBlackFriday: false,
    sealExclusive: false,
    sealMarketing: false,
    description:
      '<article><h2>Por que o Plafon Embutido Quadrado 3541?</h2><p> Precisando de uma nova <strong>iluminação</strong> para o quarto, sala ou outro ambiente de sua casa? Que tal adquirir um produto de qualidade como o <strong>Plafon Embutido Quadrado 3541</strong>? Seu modelo é fácil de combinar com qualquer espaço, proporcionando uma <strong>claridade suave</strong> e agradável. Não deixe a oportunidade passar, leva já! :)</p></article>',
    seal_3d: false,
    sold_and_delivered_by: false,
    price: null,
    finalPrice: '179,99',
    specialPrice: 0,
    installments: {
      count: 3,
      value: '60,00',
    },
    productImage: {
      optionTwo: {
        main: '//static.mobly.local/r/341x341/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
        sprite:
          '//static.mobly.local/r/341x341/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-sprite',
      },
      optionThree: {
        main: '//static.mobly.local/r/304x304/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
        sprite:
          '//static.mobly.local/r/304x304/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-sprite',
      },
      optionFour: {
        main: '//static.mobly.local/r/222x222/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
        sprite:
          '//static.mobly.local/r/222x222/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-sprite',
      },
      collection: [
        {
          optionTwo:
            '//static.mobly.local/r/341x341/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
          optionThree:
            '//static.mobly.local/r/304x304/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
          optionFour:
            '//static.mobly.local/r/222x222/p/Jd-Molina-Plafon-Embutido-Quadrado-3541-1-LC3A2mpada--Bivolt-3078-351122-1',
        },
      ],
    },
    productAttribute: {
      color: {
        color: 'Branco',
        label: 'Cor',
      },
      weight: {
        product_weight: '1040.000',
      },
      height: {
        product_height: '3',
      },
      width: {
        product_width: '34',
      },
      length: {
        product_length: '34',
      },
      product_warranty: {
        product_warranty: '3 Meses',
        label: 'Garantia',
      },
      description: {
        description:
          '<article><h2>Por que o Plafon Embutido Quadrado 3541?</h2><p> Precisando de uma nova <strong>iluminação</strong> para o quarto, sala ou outro ambiente de sua casa? Que tal adquirir um produto de qualidade como o <strong>Plafon Embutido Quadrado 3541</strong>? Seu modelo é fácil de combinar com qualquer espaço, proporcionando uma <strong>claridade suave</strong> e agradável. Não deixe a oportunidade passar, leva já! :)</p></article>',
        label: 'Descrição',
      },
      product_contents: {
        product_contents: '1 Embutido Montado; Transformador; 1 Lâmpada Led',
        label: 'Conteúdo da Embalagem',
      },
      application: {
        application: 'Sala de Estar',
        label: 'Ambientes',
      },
      model: {
        model: 'Spot Embutido 3541',
        label: 'Modelo',
      },
      material: {
        material: 'Alumínio,Latão',
        label: 'Material',
      },
      size_description: {
        size_description: 'Altura 3 cm Largura 34 cm Comprimento 34 cm',
        label: 'Descrição do Tamanho',
      },
      product_height: {
        product_height: '3',
        label: 'Altura do Produto (cm)',
      },
      product_width: {
        product_width: '34',
        label: 'Largura do Produto (cm)',
      },
      product_length: {
        product_length: '34',
        label: 'Comprimento do Produto (cm)',
      },
      product_weight: {
        product_weight: '1040.000',
        label: 'Peso do Produto (Kg)',
      },
      assembly_required: {
        assembly_required: 'Não',
        label: 'Necessita Montagem?',
      },
      light_lamp_type: {
        light_lamp_type: 'LED',
        label: 'Tipo de Lâmpada',
      },
      format_gl: {
        format_gl: 'Quadrado',
        label: 'Formato',
      },
      watt: {
        watt: '20 - 49W',
        label: 'Potência (W)',
      },
      socket_type: {
        socket_type: 'E27',
        label: 'Tipo do Soquete',
      },
    },
    bucket: 2,
    decomposableBundle: false,
    groupCollection: [],
    hasComplementary: 0,
    immediateDelivery: false,
    ratingAggregatedTotal: false,
    productVarsPrice: '179,99',
    rawPrice: '179.99',
    extraClass: '',
    stockAvailable: 0,
    buyAndEarn: 0,
    new: false,
    starPercent: 0,
    savingPercentage: null,
    sealInternational: false,
    sealSaldaoMobly: false,
    special_price_paypal: 0,
    saving_percentage_paypal: 0,
    saving_difference_paypal: 0,
    saving_difference_spall_source: 0,
    cartRules: {
      pixDiscount: {
        active: false,
      },
      creditCard1xDiscount: {
        active: false,
      },
      lowestCartRulesByPrice: {
        hasCartRulesActive: false,
        lowestPriceAvailable: 0,
        disclaimerTxt: '',
      },
    },
    position: 2,
  },
};
