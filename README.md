# jetmobly
ATENTION!!! See [documentation](https://helpdeskmobly.atlassian.net/wiki/spaces/squadwebshop/pages/2864054280/JetMobly+-+Vitrines).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
